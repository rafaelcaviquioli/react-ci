module.exports = {
    "extends": "airbnb",
    "plugins": ["jest", "react"],
    "globals": {
        /* Global variables of Codeceptjs*/
        "Feature": true,
        "Before": true,
        "Scenario": true,
        "within": true,
        "actor": true,
        "I": true,
    },
    "rules": {
        "indent": [2, 4],
        "max-len": [1, 250],
        "react/jsx-indent-props": [1, 4],
        "react/jsx-indent": [1, 4],
        "react/prop-types": 0,
        "jest/no-disabled-tests": "warn",
        "jest/no-focused-tests": "error",
        "jest/no-identical-title": "error",
        "jest/valid-expect": "error",
        "jest/no-disabled-tests": 0,
        "jsx-a11y/anchor-is-valid": 0,
        "jsx-a11y/click-events-have-key-events": 0,
        "jsx-a11y/no-static-element-interactions": 0,
        "import/no-extraneous-dependencies": ["error", { "devDependencies": true }],
        "no-return-assign": 0,
        "linebreak-style": 0,
        "import/prefer-default-export": ["off"],
        "react/jsx-filename-extension": [1, { "extensions": [".jsx"] }],
        "react/forbid-prop-types": 0,
        "jsx-a11y/label-has-for": [2, { "allowChildren": true }],
        "react/prefer-stateless-function": [2, { "ignorePureComponents": true }]
    },
    "env": {
        "jest/globals": true,
        "browser": true
    },
    "settings": {
        "import/resolver": {
            "webpack": {
                "config": "webpack.config.js"
            }
        }
    },
};